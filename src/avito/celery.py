import os
from datetime import timedelta

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'avito.settings')

app = Celery('avito')

app.config_from_object("django.conf:settings", namespace='CELERY')

app.autodiscover_tasks(packages=['main'])

app.conf.beat_schedule = {
    'users_site': {
        'task': 'main.tasks.users_site',
        'schedule': timedelta(seconds=10),
        'args': (),
    }
}