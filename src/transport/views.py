from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, DetailView

from main.views import AnnouncementCreateView, AnnouncementListView
from transport.forms import AutoForm, SpecialTransportForm, MotorcycleForm
from transport.models import AutoModel, SpecialTransportModel, MotorcycleModel
from main.services import get_model_search


class AutoListView(AnnouncementListView):
    model = AutoModel
    template_name = 'transport/auto_list.html'


class AutoDetailView(DetailView):
    model = AutoModel
    template_name = 'transport/auto_details.html'


class SpecialTransportListView(AnnouncementListView):
    model = SpecialTransportModel
    template_name = 'transport/special_list.html'


class SpecialTransportDetailView(DetailView):
    model = SpecialTransportModel
    template_name = 'transport/special_details.html'


class MotorcycleListView(AnnouncementListView):
    model = MotorcycleModel
    template_name = 'transport/moto_list.html'


class MotorcycleDetailView(DetailView):
    model = MotorcycleModel
    template_name = 'transport/moto_details.html'


class AutoCreateView(AnnouncementCreateView):
    form_class = AutoForm
    template_name = 'main/announcement_create.html'

    # def post(self, request, *args, **kwargs):
    #     print('----------------------------------------------------------------------------')
    #     print(request.POST)
    #     form = self.get_form()
    #     print(form.errors)
    #     super().post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('auto')


class SpecialAutoCreateView(AnnouncementCreateView):
    form_class = SpecialTransportForm
    template_name = 'main/announcement_create.html'

    def get_success_url(self):
        return reverse_lazy('special')


class MotorcycleCreateView(AnnouncementCreateView):
    form_class = MotorcycleForm
    template_name = 'main/announcement_create.html'

    def get_success_url(self):
        return reverse_lazy('moto')
