from django.contrib import admin

from main.admin import AnnouncementModelAdmin
from transport.models import *


@admin.register(AutoModel, SpecialTransportModel)
class TransportModelAdmin(AnnouncementModelAdmin):
    list_display = ('id', 'announcement', 'mark', 'model', 'city', 'price', 'is_owner')


admin.site.register(MotorcycleModel, AnnouncementModelAdmin)
admin.site.register(MarksModel)
admin.site.register(AutoTypeModel)
