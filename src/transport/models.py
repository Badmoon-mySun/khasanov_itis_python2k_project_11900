from django.db import models
from django.urls import reverse

from main.models import AnnouncementAbstractModel


class MarksModel(models.Model):
    name = models.CharField(max_length=60, verbose_name='Марка')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Марка'
        verbose_name_plural = 'Марки'
        ordering = ['name']


class AutoTypeModel(models.Model):
    name = models.CharField(max_length=60, verbose_name='Модель')
    mark = models.ForeignKey(MarksModel, on_delete=models.CASCADE, verbose_name='Марка')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Модель'
        verbose_name_plural = 'Модели'
        ordering = ['name']


MILEAGE_CHOICES = [
    ('broken', 'Битый'),
    ('not broken', 'Не битый')
]


class TransportAbstractModel(AnnouncementAbstractModel):
    # Appearance
    color = models.CharField(max_length=25, blank=False, null=False, verbose_name='Цвет техники')

    # Specifications
    mark = models.ForeignKey(MarksModel, on_delete=models.CASCADE, null=False, blank=False, verbose_name='Марка')
    year_of_issue = models.PositiveIntegerField(null=False, blank=False, verbose_name='Год выпуска')
    modification = models.CharField(max_length=30, null=False, blank=False, verbose_name='Объем и мощность двигателя')
    transmission = models.CharField(max_length=30, null=False, blank=False, verbose_name='Коробка передач')
    equipment = models.CharField(max_length=30, default='Базовая', verbose_name='Комплектация')

    # Operating history and condition
    mileage = models.CharField(max_length=30, choices=MILEAGE_CHOICES, blank=False, null=False, verbose_name='Состояние')
    owners = models.PositiveIntegerField(blank=False, null=False, verbose_name='Владельцев по ПТС')

    class Meta:
        abstract = True


class AutoModel(TransportAbstractModel):
    model = models.ForeignKey(AutoTypeModel, on_delete=models.CASCADE, null=False, blank=False, verbose_name='Модель')

    def get_absolute_url(self):
        return reverse('auto_detail', kwargs={'pk': self.id})

    def __str__(self):
        return '%s %s, %s' % (self.mark, self.model, self.year_of_issue)

    class Meta:
        ordering = ['-id']
        verbose_name = 'Автомобиль'
        verbose_name_plural = 'Автомобили'


class SpecialTransportModel(TransportAbstractModel):
    model = models.CharField(max_length=15, null=False, blank=False, verbose_name='Модель')
    body_type = models.CharField(max_length=15, null=False, blank=False, verbose_name='Тип кузова')
    engine_hours = models.PositiveIntegerField(null=False, blank=False, verbose_name='Моточасы')

    def get_absolute_url(self):
        return reverse('special_detail', kwargs={'pk': self.id})

    def __str__(self):
        return '%s %s, %s' % (self.mark, self.model, self.year_of_issue)

    class Meta:
        ordering = ['-id']
        verbose_name = 'Спец Техника'
        verbose_name_plural = 'Спец Техника'


class MotorcycleModel(AnnouncementAbstractModel):
    engine_hours = models.PositiveIntegerField(null=False, blank=False, verbose_name='Моточасы')

    def get_absolute_url(self):
        return reverse('moto_detail', kwargs={'pk': self.id})

    class Meta:
        ordering = ['-id']
        verbose_name = 'Мотоцикл'
        verbose_name_plural = 'Мотоциклы'
