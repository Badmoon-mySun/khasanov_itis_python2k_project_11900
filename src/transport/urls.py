from django.urls import path, re_path

from transport.views import *

urlpatterns = [
    path('auto/', AutoListView.as_view(), name='auto'),
    path('special/', SpecialTransportListView.as_view(), name='special'),
    path('moto/', MotorcycleListView.as_view(), name='moto'),

    re_path(r'^auto/(?P<pk>\d+)/$', AutoDetailView.as_view(), name='auto_detail'),
    re_path(r'^special/(?P<pk>\d+)/$', SpecialTransportDetailView.as_view(), name='special_detail'),
    re_path(r'^moto/(?P<pk>\d+)/$', MotorcycleDetailView.as_view(), name='moto_detail'),

    path('auto/create/', AutoCreateView.as_view(), name='create_auto'),
    path('special/create/', SpecialAutoCreateView.as_view(), name='create_special'),
    path('moto/create/', MotorcycleCreateView.as_view(), name='create_moto'),
]
