from main.forms import AnnouncementCreateForm
from transport.models import SpecialTransportModel, AutoModel, MotorcycleModel


class SpecialTransportForm(AnnouncementCreateForm):
    class Meta:
        model = SpecialTransportModel
        exclude = ['user']


class AutoForm(AnnouncementCreateForm):
    class Meta:
        model = AutoModel
        exclude = ['user']


class MotorcycleForm(AnnouncementCreateForm):
    class Meta:
        model = MotorcycleModel
        exclude = ['user']
