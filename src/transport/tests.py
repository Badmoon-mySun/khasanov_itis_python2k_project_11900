from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from main.models import CityModel
from transport.models import AutoModel, MarksModel, AutoTypeModel

User = get_user_model()


class AutoListViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        test_user = User.objects.create_user(name='Test', email='test@test.com', phone='12345678', password='123')
        test_user.save()

        test_mark = MarksModel.objects.create(name='TEST')
        test_mark.save()

        test_auto_model = AutoTypeModel(mark_id=test_mark.id, name="TEST")
        test_auto_model.save()

        test_city = CityModel.objects.create(name='Казань')
        test_city.save()

        number_of_auto = 13
        for auto_num in range(number_of_auto):
            AutoModel.objects.create(announcement='Test name %s' % auto_num, user=test_user, mark=test_mark,
                                     city=test_city, model=test_auto_model, is_owner=True,
                                     description="Good", price=auto_num, year_of_issue=auto_num, owners=auto_num)

    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('auto'))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('auto'))
        self.assertEqual(resp.status_code, 200)

        self.assertTemplateUsed(resp, 'transport/auto_list.html')

    def test_pagination_is_ten(self):
        resp = self.client.get(reverse('auto'))
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('is_paginated' in resp.context)
        self.assertTrue(len(resp.context['object_list']) == 5)
        self.assertTrue(resp.context['is_paginated'])

    def test_lists_all_authors(self):
        # Get second page and confirm it has (exactly) remaining 3 items
        resp = self.client.get(reverse('auto') + '?page=3')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('is_paginated' in resp.context)
        self.assertTrue(len(resp.context['object_list']) == 3)
        self.assertTrue(resp.context['is_paginated'])


class AutoCreateViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        test_user = User.objects.create_user(name='Test', email='test@test.com', phone='123', password='123')
        test_user.save()
        test_mark = MarksModel.objects.create(name='TEST MARK')
        test_mark.save()
        test_auto_model = AutoTypeModel(mark_id=test_mark.id, name="TEST")
        test_auto_model.save()
        test_city = CityModel.objects.create(name='Казань')
        test_city.save()

        cls.valid_auto = {
            'announcement': 'Test name',
            'user': test_user.id,
            'mark': test_mark.id,
            'city': test_city.id,
            'model': test_auto_model.id,
            'is_owner': True,
            'description': 'Gooda',
            'price': 10,
            'year_of_issue': 2,
            'owners': 1,
            'color': 'test',
            'modification': 'tests',
            'transmission': 'test',
            'mileage': 'broken',
            'equipment': 'TEST'
        }

    def test_published_auto(self):
        self.client.login(username='test@test.com', password='123')
        response = self.client.post(reverse('create_auto'), self.valid_auto)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(AutoModel.objects.last().announcement, "Test name")

    def test_display_auto(self):
        self.client.login(username='test@test.com', password='123')

        self.client.post(reverse('create_auto'), self.valid_auto)

        response = self.client.get(reverse('auto_detail', kwargs={'pk': 1}))

        self.assertEqual(response.status_code, 200)

