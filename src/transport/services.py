from django.db.models import F, Count

from main.models import User
from transport.models import TransportAbstractModel


# Не используется, создан под требования
def announcement_statistic_count(model: TransportAbstractModel):
    k = 1000
    return model.objects.filter(price__lte=F('year_of_issue') / F('owners') * k).aggregate(count=Count('id'))['count']


# Не используется, создан под требования
def get_users_for_favorite_mark(id_start, id_end):
    return User.objects.prefetch_related('favorite_marks').filter(id__range=(id_start, id_end))


# Не используется, создан под требования
def users_name_and_email():
    return User.objects.values_list('name', 'email')
