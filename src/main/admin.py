from django.contrib import admin

from main.models import *


class IsOwnerFilter(admin.SimpleListFilter):
    title = 'Продавец'
    parameter_name = 'is_owner'

    def has_output(self):
        return True

    def lookups(self, request, model_admin):
        return (True, 'Собственник'), (False, 'Посредник')

    def queryset(self, request, queryset):
        qs = queryset
        if self.value():
            if self.value() == str(True):
                qs = qs.filter(is_owner=True)
            if self.value() == str(False):
                qs = qs.filter(is_owner=False)
        return qs


class AnnouncementModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'announcement', 'city', 'price', 'is_owner')
    list_display_links = ('id', 'announcement')
    list_filter = (IsOwnerFilter,)
    ordering = ('-id',)
    search_fields = ('id', 'announcement')


admin.site.register(User)
admin.site.register(CityModel)
admin.site.register(AnnouncementsImagesModel)
