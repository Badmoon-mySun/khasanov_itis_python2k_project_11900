from django.db.models import Q

from main.models import AnnouncementAbstractModel


def get_model_search(model: AnnouncementAbstractModel, search='', city=''):
    return model.objects.select_related('city').filter(
        Q(announcement__icontains=search, city__name__icontains=city))
