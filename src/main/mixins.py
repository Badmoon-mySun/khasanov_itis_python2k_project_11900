from django.conf import settings
from django.contrib.auth.mixins import AccessMixin
from django.core.exceptions import ImproperlyConfigured
from django.shortcuts import redirect


class AnonymousRequiredMixin(AccessMixin):
    """Verify that the current user is not authenticated."""

    home_url = None

    def get_home_url(self):
        home_url = self.home_url or settings.LOGIN_REDIRECT_URL
        if not home_url:
            raise ImproperlyConfigured(
                '{0} is missing the home_url attribute. Define {0}.home_url, settings.LOGIN_REDIRECT_URL.'.format(
                    self.__class__.__name__)
            )
        return str(home_url)

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(self.get_home_url())
        return super().dispatch(request, *args, **kwargs)
