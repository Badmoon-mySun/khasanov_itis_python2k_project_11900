from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, PasswordResetForm
from django.template import loader

from main.models import User,  AnnouncementsImagesModel
from main.tasks import celery_send_email
from transport.models import SpecialTransportModel


class CustomPasswordResetForm(PasswordResetForm):
    def send_mail(self, subject_template_name, email_template_name,
                  context, from_email, to_email, html_email_template_name=None):

        subject = loader.render_to_string(subject_template_name, context)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(email_template_name, context)

        celery_send_email.delay(subject, body, from_email, [to_email])


class ForgotPasswordForm(forms.Form):
    username = forms.EmailField(label='Email')


class CustomAuthenticationForm(AuthenticationForm):
    username = forms.EmailField(label='Email')


class CustomUserCreationForm(UserCreationForm):
    name = forms.CharField(label='Имя', max_length=15)

    class Meta:
        model = User
        fields = {'email', 'name'}


class AnnouncementCreateForm(forms.ModelForm):
    images = forms.FileField(widget=forms.FileInput(attrs={'multiple': True}), required=False)

    def __init__(self, user: User, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user

    def save(self, commit=True):
        self.instance.user = self.user

        super(AnnouncementCreateForm, self).save(commit)

        if commit:
            for img in self.files.getlist('images'):
                image = AnnouncementsImagesModel(announcement_id=self.instance.id, image=img)
                image.save()

    class Meta:
        model = SpecialTransportModel
        exclude = ['user']
