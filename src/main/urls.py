from django.contrib.auth.views import PasswordResetView
from django.urls import path, include

from main.views import *

urlpatterns = [
    path('', home_view, name='home'),
    path('login/', UserLoginView.as_view(), name='login'),
    path('registration/', UserRegistrationView.as_view(), name='registration'),
    path('profile/', UserProfileView.as_view(), name='profile'),
    path('profile/edit/', UserProfileEditView.as_view(), name='profile_edit'),
    path('profile/password/reset/', PasswordResetView.as_view(), name='reset_password'),
    path('accounts/password_reset/', CustomPasswordResetView.as_view(), name='password_reset'),
    path('accounts/', include('django.contrib.auth.urls')),
]

