from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, PasswordResetView
from django.core.mail import send_mail
from django.shortcuts import redirect, get_object_or_404, render
from django.urls import reverse_lazy
from django.views.generic import DetailView, UpdateView, CreateView, ListView

from avito import settings
from main.forms import CustomUserCreationForm, CustomAuthenticationForm, AnnouncementCreateForm, ForgotPasswordForm, \
    CustomPasswordResetForm
from main.mixins import AnonymousRequiredMixin
from main.models import User
from main.services import get_model_search


class UserLoginView(LoginView):
    form_class = CustomAuthenticationForm
    template_name = 'main/login.html'
    success_url = '/'


class UserRegistrationView(AnonymousRequiredMixin, CreateView):
    form_class = CustomUserCreationForm
    template_name = 'main/registration.html'
    success_url = reverse_lazy('home')


class UserProfileEditView(LoginRequiredMixin, UpdateView):
    template_name = 'main/user_profile_edit.html'
    fields = {'name', 'email', 'avatar'}
    success_url = reverse_lazy('profile')

    def get_object(self, queryset=None):
        return get_object_or_404(User, id=self.request.user.id)


class UserProfileView(LoginRequiredMixin, DetailView):
    template_name = 'main/user_profile.html'

    def get_object(self, queryset=None):
        return get_object_or_404(User, id=self.request.user.id)


class CustomPasswordResetView(PasswordResetView):
    form_class = CustomPasswordResetForm


@login_required
def home_view(request):
    return render(request, 'main/layouts/main.html')


class AnnouncementCreateView(LoginRequiredMixin, CreateView):
    def get_form(self, form_class=None):
        """Return an instance of the form to be used in this view."""
        if form_class is None:
            form_class = self.get_form_class()
        if not issubclass(form_class, AnnouncementCreateForm):
            raise ValueError('form class are not implement AnnouncementCreateForm class')
        return form_class(self.request.user, **self.get_form_kwargs())


class AnnouncementListView(ListView):
    extra_context = {}
    paginate_by = 5

    def get_queryset(self):
        search = self.request.GET.get('search')
        city = self.request.GET.get('city', 'Казань')

        self.extra_context['city'] = city if city else ''
        self.extra_context['search'] = search if search else ''

        return get_model_search(self.model, self.extra_context['search'], self.extra_context['city'])
