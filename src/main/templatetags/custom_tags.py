from django import template
register = template.Library()


@register.filter
def name_of_form_model(form):
    return form._meta.model._meta.verbose_name_plural


@register.filter
def verbose_name_plural(model):
    return model._meta.verbose_name_plural
