import logging

from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.core.mail import send_mail

from avito.celery import app

User = get_user_model()

token_generator = PasswordResetTokenGenerator()


@app.task
def celery_send_email(subject, body, from_email, to_email):
    send_mail(subject, body, from_email, to_email)


logger = logging.getLogger(__name__)


@app.task
def users_site():
    # Сделать рассылку всем пользователям email
    print('In deferred task')
    users = User.objects.all()
    logger.info(f'All users: {len(users)} on sites')

