from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, UserManager as DjangoUserManager
from django.core.validators import MinValueValidator
from djchoices import DjangoChoices, ChoiceItem
from django.db import models

from avito import settings


class CityModel(models.Model):
    name = models.CharField(max_length=60, verbose_name="Название города")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'


class AnnouncementAbstractModel(models.Model):
    announcement = models.CharField(max_length=60, blank=False, null=False, verbose_name='Название объявления')
    is_owner = models.BooleanField(blank=False, null=False, verbose_name='Собственник?')
    youtube = models.URLField(null=True, blank=True, verbose_name='Ссылка на YouTube')
    description = models.TextField(max_length=1500, blank=False, verbose_name='Описание')
    city = models.ForeignKey(CityModel, on_delete=models.CASCADE, blank=False, null=False, verbose_name='Город')
    price = models.PositiveIntegerField(verbose_name='Цена')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=False, blank=False, verbose_name='Продавец')
    create_date = models.DateTimeField(auto_now=True, null=True, verbose_name='Дата создания')

    def __str__(self):
        return self.announcement


class AnnouncementsImagesModel(models.Model):
    announcement = models.ForeignKey(AnnouncementAbstractModel, on_delete=models.CASCADE, related_name='images',
                                     verbose_name="Объявление")
    image = models.ImageField(upload_to='announcements/', verbose_name='Фото объявления')

    def get_absolute_url(self):
        return self.image.url

    def __str__(self):
        return 'Фотография объявления'

    class Meta:
        verbose_name = 'Фотография объявления'
        verbose_name_plural = 'Фотографии объявлений'


class UserManager(DjangoUserManager):
    def create_user(self, email, name=None, password=None, **kwargs):
        user = self.model(email=email, name=name)
        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, email, name=None, password=None, **kwargs):
        user = self.model(email=email, name=name, is_superuser=True)
        user.set_password(password)
        user.save()

        return user


class Role(DjangoChoices):
    admin = ChoiceItem()
    user = ChoiceItem()


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=15, blank=False, null=False)
    role = models.CharField(choices=Role.choices, default=Role.user, max_length=50)
    avatar = models.ImageField(upload_to='avatars/', default=None, blank=True)
    phone = models.CharField(max_length=20, blank=False, null=False, verbose_name='Телефон')
    birthday = models.DateField(null=True, blank=True, verbose_name='День рождения')
    favorite_marks = models.ManyToManyField('transport.MarksModel', verbose_name='Любимые марки')
    is_active = models.BooleanField(default=True, verbose_name='Активен?')

    objects = UserManager()

    @property
    def is_staff(self):
        return self.is_superuser

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name', 'phone']

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class PositiveFloatField(models.FloatField):
    def __init__(self, **kwargs):
        super().__init__(kwargs, validators=[MinValueValidator(.0)])
