from django.urls import path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework.routers import SimpleRouter

from api.views import AutoViewSet, MainApiView

router = SimpleRouter()
router.register('auto', AutoViewSet, 'auto_api')

schema_view = get_schema_view(
   openapi.Info(
      title="Avito API",
      default_version='v1',
      description="This is avito api",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="anvar00755@mail.ru"),
      license=openapi.License(name="ANVAR License"),
   ),
   public=True,
)

urlpatterns = [
   path('', MainApiView.as_view(), name='main_api_view'),
   *router.urls,
   re_path('swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
   path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
   path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
