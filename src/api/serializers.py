from rest_framework import serializers

from main.models import User, AnnouncementsImagesModel, CityModel
from transport.models import AutoModel, MarksModel, AutoTypeModel


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'name', 'email', 'phone']
        read_only_fields = ['id', 'name', 'email', 'phone']


class MarksSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarksModel
        fields = ['id', 'name']
        read_only_fields = ['id', 'name']


class ModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = AutoTypeModel
        fields = ['id', 'name']
        read_only_fields = ['id', 'name']


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnnouncementsImagesModel
        fields = ['id', 'image']
        read_only_fields = ['id', 'image']


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = CityModel
        fields = ['id', 'name']
        read_only_fields = ['id', 'name']


class AutoSerializer(serializers.ModelSerializer):
    user_id = UserSerializer()
    mark_id = MarksSerializer()
    model_id = ModelSerializer()
    city_id = CitySerializer()
    images = ImageSerializer(many=True)

    class Meta:
        model = AutoModel
        fields = '__all__'


class AutoCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = AutoModel
        fields = '__all__'
