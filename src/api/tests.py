import json

from django.contrib.auth import get_user_model, authenticate
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from api.serializers import AutoSerializer
from main.models import CityModel
from transport.models import MarksModel, AutoModel, AutoTypeModel

User = get_user_model()


class ApiTests(APITestCase):
    def setUp(self):
        test_user = User.objects.create_user(name='Test', email='test@test.com', phone='12345678', password='123')
        test_user.save()

        self.client.login(username=test_user.email, password='123')

        test_mark = MarksModel.objects.create(name='TEST')
        test_mark.save()

        test_auto_model = AutoTypeModel(mark_id=test_mark.id, name="TEST")
        test_auto_model.save()

        test_city = CityModel.objects.create(name='TEST')
        test_city.save()

        self.test_auto = AutoModel.objects.create(announcement='Test name', user=test_user, mark=test_mark,
                                                  city=test_city, model=test_auto_model, is_owner=True,
                                                  description="Good", price=10, year_of_issue=2, owners=1)
        self.test_auto.save()

        self.valid_auto = {
            'announcement': 'Test name',
            'user': test_user.id,
            'mark': test_mark.id,
            'city': test_city.id,
            'model': test_auto_model.id,
            'is_owner': True,
            'description': 'Good',
            'price': 10,
            'year_of_issue': 2,
            'owners': 1,
            'color': 'test',
            'modification': 'test',
            'transmission': 'test',
            'mileage': 'broken'
        }

        self.valid_auto_update = {
            'announcement': 'Test name 2',
        }

    def test_get_auto(self):
        response = self.client.get(reverse('auto_api-detail', kwargs={'pk': self.test_auto.id}), format='json')
        auto = AutoSerializer(self.test_auto).data
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, auto)

    def test_fail_get_auto(self):
        response = self.client.get(reverse('auto_api-detail', kwargs={'pk': 999}), format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_post_auto(self):
        response = self.client.post(path='/api/auto/', data=json.dumps(self.valid_auto),
                                    content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_auto(self):
        response = self.client.put(path=f'/api/auto/{self.test_auto.id}',
                                   data=json.dumps(self.valid_auto_update),
                                   content_type='application/json')

        self.assertEqual(response.status_code, status.HTTP_301_MOVED_PERMANENTLY)

    def test_delete_auto(self):
        response = self.client.delete(path=f'/api/auto/{self.test_auto.id}')

        self.assertEqual(response.status_code, status.HTTP_301_MOVED_PERMANENTLY)
