from rest_framework import viewsets, status
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import BasePermission, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.serializers import AutoSerializer, AutoCreateSerializer
from transport.models import AutoModel


class MainApiView(APIView):
    """Class for checking api"""
    def get(self, request):
        return Response({'status': 'ok'})


class SiteChangeOnlyForOwnerPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if not request.user.is_authenticated:
            return False
        return obj.user.id == request.user.id


class SmallResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100


class AutoViewSet(viewsets.ModelViewSet):
    """Авто"""
    pagination_class = SmallResultsSetPagination
    permission_classes = [IsAuthenticated, SiteChangeOnlyForOwnerPermission]
    serializer_class = AutoSerializer
    queryset = AutoModel.objects.all()

    def get_queryset(self):
        return AutoModel.objects.select_related('user', 'mark', 'model', 'city')

    def list(self, request, *args, **kwargs):
        """Выводит список авто"""
        return super(AutoViewSet, self).list(request, *args, **kwargs)

    def create(self, request, post_id=None, *args, **kwargs):
        serializer = AutoCreateSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()

            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

        return Response(status=403, data='Not valid')
