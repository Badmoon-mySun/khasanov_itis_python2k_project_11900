from django.urls import path, re_path

from real_estate.views import HouseCreateView, ApartmentHouseCreateView, \
    LandPlotHouseCreateView, HouseListView, HouseDetailView

urlpatterns = [
    path('house/', HouseListView.as_view(), name='house'),
    path('apartment/', HouseListView.as_view(), name='apartment'),
    path('landplot/', HouseListView.as_view(), name='landplot'),

    re_path(r'^house/(?P<pk>\d+)/$', HouseDetailView.as_view(), name='house_detail'),
    re_path(r'^apartment/(?P<pk>\d+)/$', HouseDetailView.as_view(), name='apartment_detail'),
    re_path(r'^landplot/(?P<pk>\d+)/$', HouseDetailView.as_view(), name='landplot_detail'),

    path('house/create/', HouseCreateView.as_view(), name='create_house'),
    path('apartment/create/', ApartmentHouseCreateView.as_view(), name='create_apartment'),
    path('landplot/create/', LandPlotHouseCreateView.as_view(), name='create_landplot'),
]
