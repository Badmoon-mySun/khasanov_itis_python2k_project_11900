from django.contrib import admin

from main.admin import AnnouncementModelAdmin
from real_estate.models import *


admin.site.register(HouseModel, AnnouncementModelAdmin)
admin.site.register(ApartmentModel, AnnouncementModelAdmin)
admin.site.register(LandPlotModel, AnnouncementModelAdmin)
