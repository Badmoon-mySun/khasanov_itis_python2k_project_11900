from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from main.models import CityModel
from real_estate.models import HouseModel

User = get_user_model()


class HomeListViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        test_user = User.objects.create_user(name='Test', email='test@test.com', phone='54321', password='123')
        test_user.save()
        test_city = CityModel.objects.create(name='Казань')
        test_city.save()

        number_of_home = 13
        for house_num in range(number_of_home):
            HouseModel.objects.create(announcement='Test name %s' % house_num, user=test_user, city=test_city,
                                      is_owner=True, description="Good", price=house_num, levels=2, material='lol',
                                      house_area=12, land_area=12)

    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('house'))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('house'))
        self.assertEqual(resp.status_code, 200)

        self.assertTemplateUsed(resp, 'real_estate/house_list.html')

    def test_pagination_is_ten(self):
        resp = self.client.get(reverse('house'))
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('is_paginated' in resp.context)
        self.assertTrue(len(resp.context['object_list']) == 5)
        self.assertTrue(resp.context['is_paginated'])

    def test_lists_all_authors(self):
        resp = self.client.get(reverse('house') + '?page=3')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('is_paginated' in resp.context)
        self.assertTrue(len(resp.context['object_list']) == 3)
        self.assertTrue(resp.context['is_paginated'])


class AutoCreateViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        test_user = User.objects.create_user(name='Test', email='test@test.com', phone='123', password='123')
        test_user.save()
        test_city = CityModel.objects.create(name='Казань')
        test_city.save()

        cls.valid_house = {
            'announcement': 'Test name',
            'user': test_user.id,
            'city': test_city.id,
            'is_owner': True,
            'description': 'Gooda',
            'price': 10,
            'levels': 1,
            'material': 'lol',
            'house_area': 1,
            'land_area': 1
        }

    def test_published_auto(self):
        self.client.login(username='test@test.com', password='123')

        response = self.client.post(reverse('create_house'), self.valid_house)

        self.assertEqual(response.status_code, 302)

        self.assertEqual(HouseModel.objects.last().announcement, "Test name")

    def test_display_auto(self):
        self.client.login(username='test@test.com', password='123')

        self.client.post(reverse('create_house'), self.valid_house)

        response = self.client.get(reverse('house_detail', kwargs={'pk': 1}))

        self.assertEqual(response.status_code, 200)
