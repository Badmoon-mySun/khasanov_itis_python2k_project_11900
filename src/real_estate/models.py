from django.core.validators import MinValueValidator
from django.db import models
from django.urls import reverse

from main.models import AnnouncementAbstractModel


class PositiveFloatField(models.FloatField):
    def __init__(self, *args, **kwargs):
        kwargs['validators'] = [MinValueValidator(.0)]
        super().__init__(*args, **kwargs)


class HouseModel(AnnouncementAbstractModel):
    levels = models.PositiveSmallIntegerField(null=False, blank=False, verbose_name='Этажей в доме')
    material = models.CharField(max_length=30, null=False, blank=False, verbose_name='Материал стен')
    house_area = PositiveFloatField(null=False, blank=False, verbose_name='Площадь дома')
    land_area = PositiveFloatField(null=False, blank=False, verbose_name='Площадь участка')

    def get_absolute_url(self):
        return reverse('house_detail', kwargs={'pk': self.id})

    class Meta:
        verbose_name = 'Дом'
        verbose_name_plural = 'Дома'


HOUSE_TYPES = [
    ("Brick", "Кирпичный"),
    ("Panel", "Панельный"),
    ("Block", "Блочный"),
    ("Monolithic", "Монолитный"),
    ("Wood", "Деревянный"),
]


class ApartmentModel(AnnouncementAbstractModel):
    # about house
    house_type = models.CharField(max_length=20, choices=HOUSE_TYPES, null=False, blank=False, verbose_name='Тип дома')
    build_year = models.PositiveIntegerField(null=False, blank=False, verbose_name='Год постройки')
    is_there_lift = models.BooleanField(null=False, blank=False, verbose_name='Есть лифт?')
    levels_count = models.PositiveSmallIntegerField(null=False, blank=False, verbose_name='Этажей в доме')

    # about apartment
    level = models.PositiveSmallIntegerField(null=False, blank=False, verbose_name='Этаж')
    rooms_count = models.PositiveSmallIntegerField(null=False, blank=False, verbose_name='Количество Комнат')
    apartment_area = PositiveFloatField(null=False, blank=False, verbose_name='Площадь квартиры')

    def get_absolute_url(self):
        return reverse('apartment_detail', kwargs={'pk': self.id})

    class Meta:
        verbose_name = 'Квартира'
        verbose_name_plural = 'Квартиры'


class LandPlotModel(AnnouncementAbstractModel):
    area = PositiveFloatField(null=False, blank=False, verbose_name='Площадь')

    def get_absolute_url(self):
        return reverse('landplot_detail', kwargs={'pk': self.id})

    class Meta:
        verbose_name = 'Земельный участок'
        verbose_name_plural = 'Земельные участки'
