from django.urls import reverse_lazy
from django.views.generic import DetailView

from main.views import AnnouncementCreateView, AnnouncementListView
from real_estate.forms import HouseForm, LandPlotForm, ApartmentForm
from real_estate.models import HouseModel


class HouseListView(AnnouncementListView):
    model = HouseModel
    template_name = 'real_estate/house_list.html'


class HouseDetailView(DetailView):
    model = HouseModel
    template_name = 'real_estate/house_detail.html'


class HouseCreateView(AnnouncementCreateView):
    form_class = HouseForm
    template_name = 'main/announcement_create.html'

    def get_success_url(self):
        return reverse_lazy('house')


class LandPlotHouseCreateView(AnnouncementCreateView):
    form_class = LandPlotForm
    template_name = 'main/announcement_create.html'

    def get_success_url(self):
        return reverse_lazy('land_plot')


class ApartmentHouseCreateView(AnnouncementCreateView):
    form_class = ApartmentForm
    template_name = 'main/announcement_create.html'

    def get_success_url(self):
        return reverse_lazy('apartment')
