from main.forms import AnnouncementCreateForm
from real_estate.models import HouseModel, ApartmentModel, LandPlotModel


class HouseForm(AnnouncementCreateForm):
    class Meta:
        model = HouseModel
        exclude = ['user']


class ApartmentForm(AnnouncementCreateForm):
    class Meta:
        model = ApartmentModel
        exclude = ['user']


class LandPlotForm(AnnouncementCreateForm):
    class Meta:
        model = LandPlotModel
        exclude = ['user']
